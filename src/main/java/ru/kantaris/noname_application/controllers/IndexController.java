package ru.kantaris.noname_application.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kantaris.noname_application.model.IndexDto;

@RestController("/")
public class IndexController {

    @GetMapping
    public IndexDto getMessage(@Value("${app.version}") String version) {
        return new IndexDto("ok", version);
    }
}
